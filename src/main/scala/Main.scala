import sfml.graphics.{Font, RenderWindow, Sprite, Text, Texture, View}
import sfml.window.{Event, VideoMode}

val RESOURCES_DIR = "src/main/resources/"

@main def main =
    /* Create the main window */
    val window = RenderWindow(VideoMode(800, 600), "Hello SFML")
    window.framerateLimit = 30

    /* Load a sprite to display */
    val texture = Texture()
    if !(texture.loadFromFile(RESOURCES_DIR + "SFML.png")) then System.exit(1)

    val sprite = Sprite(texture)

    /* Create a graphical text to display */
    val font = Font()
    if !(font.loadFromFile(RESOURCES_DIR + "FiraSans.ttf")) then System.exit(1)

    val text = Text("Hello SFML", font, 50)

    /* Start the game loop */
    while window.isOpen() do
        /* Process events */
        for event <- window.pollEvent() do
            event match
                case Event.Closed()               => window.close()
                case Event.Resized(width, height) => window.view = View((0, 0, width, height))
                case _                            => ()

        /* Update the position of elements */
        sprite.position = (window.size.x / 2 - sprite.globalBounds.width / 2, 100f)
        text.position = (window.size.x / 2 - text.globalBounds.width / 2, 400f)

        /* Clear the screen */
        window.clear()

        /* Draw the sprite */
        window.draw(sprite)

        /* Draw the string */
        window.draw(text)

        /* Update the window */
        window.display()

    System.exit(0)
